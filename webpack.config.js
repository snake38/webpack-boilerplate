module.exports = {
  entry: "./src/index.js",
  output: {
    path: __dirname + '/build/',
    filename: 'app.js'
  },
  devtool: '#inline-source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /(node_modules|jspm_packages|bower_components)/,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      }
    ]
  }
};